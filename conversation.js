var ConversationV1 = require('watson-developer-cloud/conversation/v1');
var prompt =require('prompt-sync')();
//setup conversation service wrapper

var conversation = new ConversationV1({
	username:"username" ,
	password:"password",
	path: {workspace_id:"watson_id"},
	version_date: '2017-04-25'
});

//start conversation

conversation.message({},processResponse);
//conversation.message({input :{text:"i want butter chicken"}},processResponse);
 

// Process the conversation response.
function processResponse(err, response) {
  if (err) {
    console.error(err); // something went wrong
    return;
  }

  // If an intent was detected, log it out to the console.
  if (response.intents.length > 0) {
    console.log('Detected intent: #' + response.intents[0].intent);
  }
  
  // Display the output from dialog, if any.
  if (response.output.text.length != 0) {
      console.log(response.output.text[0]);
  }

  // Prompt for the next round of input.
    var newMessageFromUser = prompt('>> ');
    // Send back the context to maintain state.
    conversation.message({
      input: { text: newMessageFromUser },
      context : response.context,
    }, processResponse)
}
